# First Smart Contract Project: Inbox and Lottery Contracts

## Project Overview

This project demonstrates the development of two smart contracts, Inbox and Lottery, using Solidity. The scope of the project includes writing the smart contracts, compiling them, writing unit tests, deploying, and verifying them on the Sepolia testnet.

## Project Smart Contracts

- **Inbox Contract**: A simple smart contract that allows users to store and retrieve a message on the blockchain. You can access the Inbox contract on the Sepolia testnet [here](https://sepolia.etherscan.io/address/0xFDAABeD590a43968A2F9B44534a2ecF47D86B476).
- **Lottery Contract**: A more complex contract that allows users to enter a lottery, randomly pick a winner, and distribute the prize pool. You can access the Lottery contract on the Sepolia testnet [here](https://sepolia.etherscan.io/address/0x6eB6546B11cF1A2D5917b2E1C7F33BBd8c80A917).

## Acknowledgements

- Solidity
- Ganache CLI
- Mocha
- web3.js
- Infura
- Truffle HDWalletProvider