const HDWalletProvider = require('@truffle/hdwallet-provider')
const Web3 = require('web3')
const { abi, evm } = require('./compile')
require('dotenv').config({ path: '.env.local' });

const seedPhrase = process.env.SEED_PHRASE ?? ''
const sepoliaUrl = process.env.SEPOLIA_URL ?? ''

const provider = new HDWalletProvider(seedPhrase, sepoliaUrl)

const web3 = new Web3(provider);

const deploy = async () => {
  const accounts = await web3.eth.getAccounts()
  console.log("Used Account:", accounts[0])

  const gasEstimate = await new web3.eth.Contract(abi)
  .deploy({
    data: evm.bytecode.object,
  }).estimateGas({ from: accounts[0] })

  const deployedContract = await new web3.eth.Contract(abi)
    .deploy({
      data: evm.bytecode.object,
    })
    .send({
      from: accounts[0],
      gas: gasEstimate
    });

  console.log('Address: ', deployedContract.options.address)
  provider.engine.stop()
}

deploy()