// SPDX-License-Identifier: MIT
pragma solidity 0.8.19;

contract Lottery {
  address public owner;

  struct Participant {
    string name;
    bool isWinner;
    uint256 prizeEarned;
  }

  enum LotteryStatus { NOT_STARTED, ONGOING, ENDED}

  struct LotteryDetail {
    string name;
    LotteryStatus status;
    Participant[] participants;
    uint256 prizePool;
  }

  mapping(string => LotteryDetail) public lotteryList;
  mapping(string => bool) private lotteryExists;

  constructor() {
    owner = msg.sender;
  }

  modifier onlyOwner() {
    require(owner == msg.sender, "You can't access this function");
    _;
  }

  modifier lotteryShouldNew(string memory _lotteryName) {
    require(!lotteryExists[_lotteryName], "Lottery with this name already exists");
    _;
  }

  modifier lotteryShouldExist(string memory _lotteryName) {
    require(lotteryExists[_lotteryName], "Lottery with this name is not exists");
    _;
  }

  event LogMessage(string message);

  function createNewLottery(string memory _lotteryName, uint256 _prizePool) public onlyOwner lotteryShouldNew(_lotteryName){
    lotteryExists[_lotteryName] = true;
    lotteryList[_lotteryName].name = _lotteryName;
    lotteryList[_lotteryName].status = LotteryStatus.NOT_STARTED;
    lotteryList[_lotteryName].prizePool = _prizePool;
    emit LogMessage("New Lottery is Created");
  }

  function addParticipant(string memory _lotteryName, string memory _participantName) public lotteryShouldExist(_lotteryName) {
    require(lotteryList[_lotteryName].status == LotteryStatus.ONGOING, "Participant cant be added because the lottery isnt started or is ended already");
    
    for (uint256 i = 0; i < lotteryList[_lotteryName].participants.length; i++) {
      if (keccak256(abi.encodePacked(lotteryList[_lotteryName].participants[i].name)) == keccak256(abi.encodePacked(_participantName))) {
        revert("Participant already exists in the lottery");
      }
    }

    Participant memory newParticipant = Participant({
      name: _participantName,
      isWinner: false,
      prizeEarned: 0
    });

    lotteryList[_lotteryName].participants.push(newParticipant);
    emit LogMessage("New Participant is Added");
  }

  function startLottery(string memory _lotteryName) public onlyOwner lotteryShouldExist(_lotteryName) {
    require(lotteryList[_lotteryName].status == LotteryStatus.NOT_STARTED, "Lottery is already started or ended");

    lotteryList[_lotteryName].status = LotteryStatus.ONGOING;

    emit LogMessage("Lottery status modified");
  }

  function calculatePrize(uint256 index, uint256 prizePool) internal pure returns (uint256) {
    if (index == 0) {
        return (prizePool * 40) / 100; // 40% for the first winner
    } else if (index == 1) {
        return (prizePool * 25) / 100; // 25% for the second winner
    } else if (index == 2) {
        return (prizePool * 15) / 100; // 15% for the third winner
    } else if (index == 3) {
        return (prizePool * 10) / 100; // 10% for the fourth winner
    } else {
        return (prizePool * 10) / 100; // 10% for the fifth winner
    }
  }

  function endLottery(string memory _lotteryName) public onlyOwner {
    require(lotteryList[_lotteryName].status == LotteryStatus.ONGOING, "Lottery is not in the ONGOING state");
    LotteryDetail storage tempLottery = lotteryList[_lotteryName];
    uint256 participantCount = tempLottery.participants.length;

    if (participantCount > 0) {
      uint256 winnerCount = participantCount > 5 ? 5 : participantCount;

      for (uint256 i = 0; i < winnerCount; i++) {
        uint256 randomIndex = uint256(keccak256(abi.encodePacked(block.timestamp, block.prevrandao, i))) % participantCount;
        Participant storage winner = tempLottery.participants[randomIndex];

        // Ensure the participant hasn't already been selected as a winner
        while (winner.isWinner) {
          randomIndex = (randomIndex + 1) % participantCount;
          winner = tempLottery.participants[randomIndex];
        }

        winner.isWinner = true;
        winner.prizeEarned = calculatePrize(i, tempLottery.prizePool);
      }
    }

    tempLottery.status = LotteryStatus.ENDED;

    emit LogMessage("Lottery ended and winners selected");
  }

  function getLotteryDetail(string memory _lotteryName) public view lotteryShouldExist(_lotteryName) returns (LotteryDetail memory) {
    return lotteryList[_lotteryName];
  }
}