const path = require('path');
const fs = require('fs');
const solc = require('solc')

const solidityPath = path.resolve(__dirname, 'contract', 'Lottery.sol')
const source = fs.readFileSync(solidityPath, 'utf-8')

let input = {
  language: 'Solidity',
  sources: {
    'Lottery.sol' : {
      content: source
    }
  },
  settings: {
    outputSelection: {
      '*': {
        '*': ['*']
      }
    }
  }
}

// Compile the Solidity contract
const output = JSON.parse(solc.compile(JSON.stringify(input)));

// Export the ABI and bytecode
module.exports = {
    abi: output.contracts['Lottery.sol'].Lottery.abi,
    evm: output.contracts['Lottery.sol'].Lottery.evm
};