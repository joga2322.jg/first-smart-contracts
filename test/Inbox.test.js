const assert = require('assert');
const ganache = require('ganache-cli');
const Web3 = require('web3');
const web3 = new Web3(ganache.provider());
const { abi, evm } = require('../compile'); // Adjust the path as necessary

let accounts;
let inbox;

beforeEach(async () => {
  accounts = await web3.eth.getAccounts();

  inbox = await new web3.eth.Contract(abi)
    .deploy({
      data: evm.bytecode.object,
      arguments: ['Joshua']
    })
    .send({
      from: accounts[0],
      gas: '1000000'
    });
});

describe('Inbox testing', () => {
  it('deploy contract', () => {
    assert.ok(inbox.options.address);
  });

  it('has initial message', async () => {
    const message = await inbox.methods.message().call();
    assert.equal(message, 'Joshua');
  });

  it('can change message value', async () => {
    await inbox.methods.setMessage('Galilea').send({
      from: accounts[0]
    });
    const message = await inbox.methods.message().call();
    assert.equal(message, 'Galilea');
  });
});
