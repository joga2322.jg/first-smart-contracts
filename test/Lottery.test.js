const assert = require('assert');
const ganache = require('ganache-cli');
const Web3 = require('web3');
const web3 = new Web3(ganache.provider());
const { abi, evm } = require('../compile'); // Adjust the path as necessary

let accounts;
let lottery;

beforeEach(async () => {
  accounts = await web3.eth.getAccounts();

  let gasEstimate = await new web3.eth.Contract(abi)
  .deploy({
    data: evm.bytecode.object,
  }).estimateGas({ from: accounts[0] })
  
  lottery = await new web3.eth.Contract(abi)
    .deploy({
      data: evm.bytecode.object,
    })
    .send({
      from: accounts[0],
      gas: gasEstimate
    });
});

describe('Lottery testing', () => {
  it('deploy contract & owner address assertion', async () => {
    assert.ok(lottery.options.address);
    const ownerAddress = await lottery.methods.owner().call();
    assert.equal(ownerAddress, accounts[0]);
  });

  it('simulate whole lottery logic', async () => {
    const lotteryName = "Super Lottery"

    let gasEstimate = await lottery.methods.createNewLottery(
      lotteryName,
      2000000
    ).estimateGas({ from: accounts[0] })
    await lottery.methods.createNewLottery(
      lotteryName,
      2000000
    ).send({
      from: accounts[0],
      gas: gasEstimate
    })

    let lotteryDetail = await lottery.methods.getLotteryDetail(lotteryName).call()
    assert.equal(lotteryDetail.name, lotteryName)

    gasEstimate = await lottery.methods.startLottery(lotteryName).estimateGas({ from: accounts[0] })
    await lottery.methods.startLottery(lotteryName).send({
      from: accounts[0],
      gas: gasEstimate
    })

    gasEstimate = await lottery.methods.addParticipant(
      lotteryName,
      "Joshua Galilea"
    ).estimateGas({ from: accounts[1] })
    await lottery.methods.addParticipant(
      lotteryName,
      "Joshua Galilea"
    ).send({
      from: accounts[1],
      gas: gasEstimate
    })

    gasEstimate = await lottery.methods.addParticipant(
      lotteryName,
      "Another Joshua Galilea"
    ).estimateGas({ from: accounts[2] })
    await lottery.methods.addParticipant(
      lotteryName,
      "Another Joshua Galilea"
    ).send({
      from: accounts[2],
      gas: gasEstimate
    })

    gasEstimate = await lottery.methods.addParticipant(
      lotteryName,
      "This is Joshua Galilea"
    ).estimateGas({ from: accounts[2] })
    await lottery.methods.addParticipant(
      lotteryName,
      "This is Joshua Galilea"
    ).send({
      from: accounts[2],
      gas: gasEstimate
    })

    lotteryDetail = await lottery.methods.getLotteryDetail(lotteryName).call()
    assert.equal(lotteryDetail.participants.length, 3)
    assert.equal(lotteryDetail.participants[0].name, "Joshua Galilea")
    assert.equal(lotteryDetail.participants[1].name, "Another Joshua Galilea")
    assert.equal(lotteryDetail.participants[2].name, "This is Joshua Galilea")

    gasEstimate = await lottery.methods.endLottery(lotteryName).estimateGas({ from: accounts[0] })
    await lottery.methods.endLottery(lotteryName)
    .send({
      from: accounts[0],
      gas: gasEstimate
    })

    lotteryDetail = await lottery.methods.getLotteryDetail(lotteryName).call()
    console.log(lotteryDetail)
    
  });
});
